var ISSUER_SERVICE_CREATE_TOKEN_URL = "http://localhost:5000/issuer-services/createToken";
var ISSUER_SERVICE_DISTRIBUTE_TOKEN_URL = "http://localhost:5000/issuer-services/makePayment";
var TOKEN_DISTRIBUTON_PAGE = "security-tokens-token-distribution.html";
var TOKEN_DETAILS_PAGE = "security-tokens-token-details.html";

$(document).ready(function(){

	$('input[type=radio][name=Token-Type]').change(function() {
	    if (this.value == 'Equity') {
	        window.location.href = TOKEN_DETAILS_PAGE;
	    }
	    else if (this.value == 'transfer') {
	        alert("test");
	    }
	});


	$('#debt-type').on("change",function() {
		//alert(this.value)
	});

	$('#Currency-2').on("change",function() {
		//alert(this.value)
	});

	$('#payout-frequency').on("change",function() {
		//alert(this.value)
	});

	$("#next-btn").on("click",function() {

		var formObject = {};
		var tokenType = $('input[type=radio][name=Token-Type]').val();
		var debtType = $('#debt-type').val();	
		var principal = $('#principal').val();
		var amortizationRate = $('#amortization-rate').val();
		var payoutFreq = $('#payout-frequency').val();
		var roi = $('#rate-interest').val();
		var maturityYears = $('#number-years').val();
		var maturityMonths = $('#number-months').val();

		formObject.tokenType = tokenType;
		formObject.loanType = debtType;
		formObject.principal = principal;
		formObject.roi = roi;
		formObject.amortizationRate = amortizationRate;
		formObject.payoutFreq = payoutFreq;
		formObject.maturityYears = maturityYears;
		formObject.maturityMonths = maturityMonths;
		formObject.numberOfTokens = "";
		formObject.tokenCode = "";
		formObject.tokenLimit = "";

		if(isEmpty(debtType) || isEmpty(principal) || isEmpty(amortizationRate) || isEmpty(payoutFreq) || isEmpty(principal) || isEmpty(maturityYears) || isEmpty(maturityMonths)){
			alert("Please fill required fields !");
			return;
		}

		console.log(formObject);
		var formObjectString = JSON.stringify(formObject);
		var encodedString = btoa(formObjectString); // encoding and storing in session storage
		sessionStorage["form-data"] = encodedString;
	    window.location.href = TOKEN_DETAILS_PAGE;

	}); // end of next-btn click


	$("#create-token-btn").on("click",function() {

		var tokenCode = $('#Token-Symbol-Input').val();
		var numberOfTokens = $('#numberOfTokens').val();
		var agreeFlag = ($('#Agree').is(':checked')) ? true : false;

		if(isEmpty(tokenCode) || isEmpty(numberOfTokens)){
			alert("Please fill required fields");
			return;
		}

		if(!agreeFlag) { alert("Please agree terms & conditions"); return;}

		var formData = isEmpty(sessionStorage.length == 0) ? '' : sessionStorage["form-data"];
		var formDataObj = {};

		if(!isEmpty(formData)){
			var formDataObjStr = atob(formData);
			formDataObj = JSON.parse(formDataObjStr);
			formDataObj.tokenCode = tokenCode;
		}

		formDataObj.tokenCode = tokenCode;
		formDataObj.tokenLimit = numberOfTokens;
		console.log(formDataObj); 

		console.log("before ajax call")
		ajaxCalls(formDataObj,"POST",ISSUER_SERVICE_CREATE_TOKEN_URL).then(data => {
			    console.log(data);
			    sessionStorage["token-code"] = tokenCode;
	    		window.location.href = TOKEN_DISTRIBUTON_PAGE;
			 }).catch(error => {
			    console.log(error);
			  });
		console.log("after ajax call");

	}); // end of create-token-btn click


	$("#distribution-token-btn").on("click",function() {

		var tokenCode = isEmpty(sessionStorage.length == 0) ? '' : sessionStorage["token-code"];
		if(isEmpty(tokenCode)){
			alert("Please crate token first then try distribution")
			return;
		}

		var tokens_to_distribute = $('#tokens_to_distribute').val();
		if(isEmpty(tokens_to_distribute)){
			alert("Please fill required fields");
			return;
		}

		var formDataObj = {};
		formDataObj.tokenCode = tokenCode;
		formDataObj.tokenAmount = tokens_to_distribute;
		console.log(formDataObj); 

		console.log("before ajax call")
		ajaxCalls(formDataObj,"POST",ISSUER_SERVICE_DISTRIBUTE_TOKEN_URL).then(data => {
			    console.log(data);
 			 }).catch(error => {
			    console.log(error);
			  });
		console.log("after ajax call");

	}); // end of distribution-token-btn click

}); // end of $(document).ready()



  function ajaxCalls(data,method_type,url){

	return new Promise(function(resolve,reject){
		 $.ajax({
	      type: method_type,
	      url: url,
	      data: JSON.stringify(data),
	      dataType:"json",
	      contentType: 'application/json; charset=utf-8',
	      success: function(data, textStatus, jqXHR) { 
	      	console.log('success');
	      	console.log(data);
	      	resolve(data);
	      },
	      error : function(jqXHR, textStatus, error) { 
	      	console.log('error');
	      	console.log(error);
	      	reject(error);
	      }
		});
	}); // end of promise 
}


function isEmpty(value) {
  return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
}

